from django.apps import AppConfig


class BeersConfig(AppConfig):
    name = 'Beers'
