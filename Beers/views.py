from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from Beers.serializers import BeerTypeSerializer, BeerSerializer
from .models import BeerType, Beer

# Create your views here.


class BeerList(APIView):
    def get(self, request, format=None):
        beers = Beer.objects.all()
        serializer = BeerSerializer(beers, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = BeerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BeerTypeList(APIView):
    def get(self, request, format=None):
        beer_types = BeerType.objects.all()
        serializer = BeerTypeSerializer(beer_types, many=True)
        return Response(serializer.data)


class BeerDetails(APIView):
    def get_object(self, pk):
        try:
            return Beer.objects.get(pk=pk)
        except Beer.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        beer = self.get_object(pk)
        serializer = BeerSerializer(beer)
        return Response(serializer.data)
