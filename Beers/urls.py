from django.urls import path
from Beers.views import BeerTypeList, BeerList, BeerDetails

urlpatterns = [
    path('types/', BeerTypeList.as_view()),
    path('beers/', BeerList.as_view()),
    path('beers/<int:pk>/', BeerDetails.as_view())
]
