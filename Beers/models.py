from django.db import models


class BeerType(models.Model):

    # types of beer
    PISLNER = 'PISLNER'
    IPA = 'IPA'
    APA = 'APA'
    BOCK = 'BOCK'
    MALZBIER = 'MALZBIER'
    LAGER = 'LAGER'
    WEISS = 'WEISS'
    STOUT = 'STOUT'
    WITBIER = 'WITBIER'
    CIDER = 'CIDER'

    # Choices
    BEER_CHOICES = [
        (PISLNER, 'pislner'),
        (IPA, 'ipa'),
        (APA, 'apa'),
        (BOCK, 'bock'),
        (MALZBIER, 'malzbier'),
        (LAGER, 'lager'),
        (WEISS, 'weiss'),
        (STOUT, 'stout'),
        (WITBIER, 'witbier'),
        (CIDER, 'cider'),
    ]

    name = models.CharField(
        max_length=30, choices=BEER_CHOICES)
    characteristics = models.TextField(blank=True)
    history = models.TextField(blank=True)
    origin_country = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.name


class Beer(models.Model):
    # tipo de recipiente/ vessel
    GROWLER = 'GROWLER'
    CAN = 'CAN'
    BOTTLE = 'BOTTLE'
    BARREL = 'BARREL'

    # CHOICES
    VESSEL_CHOICES = [
        (GROWLER, 'growler'),
        (CAN, 'can'),
        (BOTTLE, 'bottle'),
        (BARREL, 'barrel')
    ]

    # fields

    beer_type = models.ForeignKey(
        BeerType, on_delete=models.CASCADE, null=True)
    brand = models.CharField(max_length=255)
    quantity = models.IntegerField(null=True)
    volume = models.CharField(max_length=100)
    price = models.FloatField(null=True)
    vessel = models.CharField(
        max_length=100, choices=VESSEL_CHOICES, default=BOTTLE)
    rating = models.IntegerField(null=True)
