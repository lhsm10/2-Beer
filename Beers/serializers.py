from rest_framework import serializers
from .models import Beer, BeerType


class BeerSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    beer_type_id = serializers.IntegerField(
        required=True)
    quantity = serializers.IntegerField(required=True)
    volume = serializers.CharField(
        required=True, max_length=100, allow_blank=False)
    price = serializers.FloatField(required=True)
    vessel = serializers.CharField(
        required=True, allow_blank=False, max_length=100)
    rating = serializers.IntegerField(required=True, min_value=1, max_value=10)

    def create(self, validated_data):
        return Beer.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.beer_type_id = validated_data.get(
            'beer_type_id', instance.beer_type_id)
        instance.quantity = validated_data.get('quantity', instance.quantity)
        instance.volume = validated_data.get('volume', instance.volume)
        instance.price = validated_data.get('price', instance.price)
        instance.vessel = validated_data.get('vessel', instance.vessel)
        instance.rating = validated_data.get('rating', instance.rating)
        instance.save()
        return instance


class BeerTypeSerializer(serializers.Serializer):
    # Read only fields
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(read_only=True, max_length=30)
    characteristics = serializers.CharField(read_only=True)
    history = serializers.CharField(read_only=True)
    origin_country = serializers.CharField(
        read_only=True, max_length=255)
